import sys
from DFWeb.models import * #required for creating DB...
from os.path import exists
from os import remove
from DFWeb import bcrypt, db, create_app
from getpass import getpass
import re 
    
def getEmail(prompt=''):
    email = input(prompt)
    regex = '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'
    if(re.search(regex,email)):  
        return email
    else:  
        print('Invalid email, try again.') 
        sys.exit()

def addDataToDB():
    username = input('Enter Username:')
    email = getEmail(prompt='Enter Email: ')
    passw = getpass(prompt='Enter Password: ')
    try:
        assert passw == getpass(prompt='Enter Password Again: ')
    except AssertionError:
        print('Passwords do not match, try again.')

    passw = bcrypt.generate_password_hash(passw)
    admin = User(username=username, email=email, password=passw)

    app = create_app()
    cont = input(f'Add {email} as Admin? (y/n)')
    with app.app_context():
        if cont.lower() == 'y':
            db.session.add(admin)

        addCerts = input('Add Data to DB? (y/n)')
        if addCerts.lower() == 'n':
            sys.exit()

        cert = Certs(filename='Analyzing Police Activity with pandas.pdf', title='Analyzing Police Activity with pandas', content='''
        Now that you have learned the foundations of pandas, this course will give you the chance to apply that knowledge by answering interesting questions about a real dataset! You will explore the Stanford Open Policing Project dataset and analyze the impact of gender on police behavior. During the course, you will gain more practice cleaning messy data, creating visualizations, combining and reshaping datasets, and manipulating time series data. Analyzing Police Activity with pandas will give you valuable experience analyzing a dataset from start to finish, preparing you for your data science career!''')
        db.session.add(cert)
        cert = Certs(filename='Cleaning Data in Python Course.pdf', title='Cleaning Data in Python', content='''
        A vital component of data science involves acquiring raw data and getting it into a form ready for analysis. It is commonly said that data scientists spend 80% of their time cleaning and manipulating data, and only 20% of their time actually analyzing it. This course will equip you with all the skills you need to clean your data in Python, from learning how to diagnose problems in your data, to dealing with missing values and outliers. At the end of the course, you'll apply all of the techniques you've learned to a case study to clean a real-world Gapminder dataset.''')
        db.session.add(cert)
        cert = Certs(filename='Data Analyst with Python Track.pdf', title='Data Analyst with Python Track', content='''
        Begin your journey into Data Science! Even if you've never written a line of code in your life, you'll be able to follow this course and witness the power of Python to perform Data Science. You'll use data to solve the mystery of Bayes, the kidnapped Golden Retriever, and along the way you'll become familiar with basic Python syntax and popular Data Science modules like Matplotlib (for charts and graphs) and Pandas (for tabular data).''')
        db.session.add(cert)
        cert = Certs(filename='Data Manipulation with Python Track.pdf', title='Data Manipulation with Python Track', content='''
        Harness the power of tools such as pandas and SQLAlchemy so you can extract, filter, and transform your data quickly and efficiently.''')
        db.session.add(cert)
        cert = Certs(filename='Data Scientist with Python Track.pdf', title='Data Scientist with Python Track', content='''
        A Data Scientist combines statistical and machine learning techniques with Python programming to analyze and interpret complex data.''')
        db.session.add(cert)
        cert = Certs(filename='Data Types for Data Science.pdf', title='Data Types for Data Science', content='''
        Have you got your basic Python programming chops down for Data Science but are yearning for more? Then this is the course for you. Herein, you'll consolidate and practice your knowledge of lists, dictionaries, tuples, sets, and date times. You'll see their relevance in working with lots of real data and how to leverage several of them in concert to solve multistep problems, including an extended case study using Chicago metropolitan area transit data. You'll also learn how to use many of the objects in the Python Collections module, which will allow you to store and manipulate your data for a variety of Data Scientific purposes. After taking this course, you'll be ready to tackle many Data Science challenges Pythonically.''')
        db.session.add(cert)
        cert = Certs(filename='Deep Learning in Python.pdf', title='Deep Learning in Python', content='''
        Deep learning is the machine learning technique behind the most exciting capabilities in diverse areas like robotics, natural language processing, image recognition, and artificial intelligence, including the famous AlphaGo. In this course, you'll gain hands-on, practical knowledge of how to use deep learning with Keras 2.0, the latest version of a cutting-edge library for deep learning in Python.''')
        db.session.add(cert)
        cert = Certs(filename='Exploratory Data Analysis in Python.pdf', title='Exploratory Data Analysis in Python', content='''
        How do we get from data to answers? Exploratory data analysis is a process for exploring datasets, answering questions, and visualizing results. This course presents the tools you need to clean and validate data, to visualize distributions and relationships between variables, and to use regression models to predict and explain. You'll explore data related to demographics and health, including the National Survey of Family Growth and the General Social Survey. But the methods you learn apply to all areas of science, engineering, and business. You'll use Pandas, a powerful library for working with data, and other core Python libraries including NumPy and SciPy, StatsModels for regression, and Matplotlib for visualization. With these tools and skills, you will be prepared to work with real data, make discoveries, and present compelling results.''')
        db.session.add(cert)
        cert = Certs(filename='Importing & Cleaning Data with Python Track.pdf', title='Importing & Cleaning Data with Python Track', content='''
        Learn to import data from various sources, such as Excel, SQL, SAS, and right from the web. From there, learn to efficiently prepare and clean your data so it is ready to by analyzed.''')
        db.session.add(cert)
        cert = Certs(filename='Importing & Managing Financial Data in Python.pdf', title='Importing & Managing Financial Data in Python', content='''
        If you want to apply your new 'Python for Data Science' skills to real-world financial data, then this course will give you some very valuable tools. First, you will learn how to get data out of Excel into pandas and back. Then, you will learn how to pull stock prices from various online APIs like Google or Yahoo! Finance, macro data from the Federal Reserve, and exchange rates from OANDA. Finally, you will learn how to calculate returns for various time horizons, analyze stock performance by sector for IPOs, and calculate and summarize correlations.''')
        db.session.add(cert)
        cert = Certs(filename='Importing Data in Python (Part 1) Course.pdf', title='Importing Data in Python (Part 1) Course', content='''
        As a data scientist, you will need to clean data, wrangle and munge it, visualize it, build predictive models, and interpret these models. Before you can do so, however, you will need to know how to get data into Python. In this course, you'll learn the many ways to import data into Python: from flat files such as .txt and .csv; from files native to other software such as Excel spreadsheets, Stata, SAS, and MATLAB files; and from relational databases such as SQLite and PostgreSQL.''')
        db.session.add(cert)
        cert = Certs(filename='Importing Data in Python (Part 2) Course.pdf', title='Importing Data in Python (Part 2) Course', content='''
        As a data scientist, you will need to clean data, wrangle and munge it, visualize it, build predictive models and interpret these models. Before you can do so, however, you will need to know how to get data into Python. In the prequel to this course, you learned many ways to import data into Python: from flat files such as .txt and .csv; from files native to other software such as Excel spreadsheets, Stata, SAS, and MATLAB files; and from relational databases such as SQLite and PostgreSQL. In this course, you'll extend this knowledge base by learning to import data from the web and by pulling data from Application Programming Interfaces— APIs—such as the Twitter streaming API, which allows us to stream real-time tweets.''')
        db.session.add(cert)
        cert = Certs(filename='Improving Your Data Visualizations in Python.pdf', title='Improving Your Data Visualizations in Python', content='''
        Great data visualization is the cornerstone of impactful data science. Visualization helps you to both find insight in your data and share those insights with your audience. Everyone learns how to make a basic scatter plot or bar chart on their journey to becoming a data scientist, but the true potential of data visualization is realized when you take a step back and think about what, why, and how you are visualizing your data. In this course you will learn how to construct compelling and attractive visualizations that help you communicate the results of your analyses efficiently and effectively. We will cover comparing data, the ins and outs of color, showing uncertainty, and how to build the right visualization for your given audience through the investigation of a datasets on air pollution around the US and farmer's markets. We will finish the course by examining open-access farmers market data to build a polished and impactful visual report.''')
        db.session.add(cert)
        cert = Certs(filename='Interactive Data Visualization with Bokeh.pdf', title='Interactive Data Visualization with Bokeh', content='''
        Bokeh is an interactive data visualization library for Python—and other languages—that targets modern web browsers for presentation. It can create versatile, data-driven graphics and connect the full power of the entire Python data science stack to create rich, interactive visualizations.''')
        db.session.add(cert)
        cert = Certs(filename='Intermediate Python for Data Science Course.pdf', title='Intermediate Python for Data Science Course', content='''
        This course extends Intermediate Python for Data Science to provide a stronger foundation in data visualization in Python. You’ll get a broader coverage of the Matplotlib library and an overview of seaborn, a package for statistical graphics. Topics covered include customizing graphics, plotting two-dimensional arrays (like pseudocolor plots, contour plots, and images), statistical graphics (like visualizing distributions and regressions), and working with time series and image data.''')
        db.session.add(cert)
        cert = Certs(filename='Intro to SQL for Data Science.pdf', title='Intro to SQL for Data Science', content='''
        The role of a data scientist is to turn raw data into actionable insights. Much of the world's raw data—from electronic medical records to customer transaction histories—lives in organized collections of tables called relational databases. To be an effective data scientist, you must know how to wrangle and extract data from these databases using a language called SQL . This course teaches syntax in SQL shared by many types of databases, such as PostgreSQL, MySQL, SQL Server, and Oracle. This course teaches you everything you need to know to begin working with databases today!''')
        db.session.add(cert)
        cert = Certs(filename='Introduction to Data Visualization with Python.pdf', title='Introduction to Data Visualization with Python', content='''
        This course extends Intermediate Python for Data Science to provide a stronger foundation in data visualization in Python. You’ll get a broader coverage of the Matplotlib library and an overview of seaborn, a package for statistical graphics. Topics covered include customizing graphics, plotting two-dimensional arrays (like pseudocolor plots, contour plots, and images), statistical graphics (like visualizing distributions and regressions), and working with time series and image data.''')
        db.session.add(cert)
        cert = Certs(filename='Introduction to Databases in Python Course.pdf', title='Introduction to Databases in Python Course', content='''
        In this course, you'll learn the basics of using SQL with Python. This will be useful because databases are ubiquitous and data scientists, analysts, and engineers must interact with them constantly. The Python SQL toolkit SQLAlchemy provides an accessible and intuitive way to query, build, and write to essential databases, including SQLite, MySQL, and PostgreSQL.''')
        db.session.add(cert)
        cert = Certs(filename='Introduction to Git for Data Science.pdf', title='Introduction to Git for Data Science', content='''
        Version control is one of the power tools of programming. It allows you to keep track of what you did when, undo any changes you decide you don't want, and collaborate at scale with other people. This course will introduce you to Git, a modern version control tool that is very popular with data scientists and software developers, and show you how to use it to get more done in less time and with less pain.''')
        db.session.add(cert)
        cert = Certs(filename='Introduction to Python Course.pdf', title='Introduction to Python Course', content='''
        Python is a general-purpose programming language that is becoming ever more popular for data science. Companies worldwide are using Python to harvest insights from their data and gain a competitive edge. Unlike other Python tutorials, this course focuses on Python specifically for data science. In our Introduction to Python course, you’ll learn about powerful ways to store and manipulate data, and helpful data science tools to begin conducting your own analyses. Start DataCamp’s online Python curriculum now.''')
        db.session.add(cert)
        cert = Certs(filename='Joining Data in SQL.pdf', title='Joining Data in SQL', content='''
        Now that you've learned the basics of SQL in our Intro to SQL for Data Science course, it's time to supercharge your queries using joins and relational set theory. In this course, you'll learn all about the power of joining tables while exploring interesting features of countries and their cities throughout the world. You will master inner and outer joins, as well as self joins, semi joins, anti joins and cross joins—fundamental tools in any PostgreSQL wizard's toolbox. Never fear set theory again after learning all about unions, intersections, and except clauses through easy-to-understand diagrams and examples. Lastly, you'll be introduced to the challenging topic of subqueries. You will be able to visually grasp these ideas by using Venn diagrams and other linking illustrations.''')
        db.session.add(cert)
        cert = Certs(filename='Linear Classiers in Python.pdf', title='Linear Classiers in Python', content='''
        In this course you'll learn all about using linear classifiers, specifically logistic regression and support vector machines, with scikit-learn. Once you've learned how to apply these methods, you'll dive into the ideas behind them and find out what really makes them tick. At the end of this course you'll know how to train, test, and tune these linear classifiers in Python. You'll also have a conceptual foundation for understanding many other machine learning algorithms.''')
        db.session.add(cert)
        cert = Certs(filename='Machine Learning with Python Track.pdf', title='Machine Learning with Python Track', content='''
        Machine learning is changing the world and if you want to be a part of the ML revolution, this is a great place to start! In this track, you’ll learn the fundamental concepts in Machine Learning.''')
        db.session.add(cert)
        cert = Certs(filename='Machine Learning with the Experts-School Budgets.pdf', title='Machine Learning with the Experts-School Budgets', content='''
        Data science isn't just for predicting ad-clicks-it's also useful for social impact! This course is a case study from a machine learning competition on DrivenData. You'll explore a problem related to school district budgeting. By building a model to automatically classify items in a school's budget, it makes it easier and faster for schools to compare their spending with other schools. In this course, you'll begin by building a baseline model that is a simple, first-pass approach. In particular, you'll do some natural language processing to prepare the budgets for modeling. Next, you'll have the opportunity to try your own techniques and see how they compare to participants from the competition. Finally, you'll see how the winner was able to combine a number of expert techniques to build the most accurate model.''')
        db.session.add(cert)
        cert = Certs(filename='Manipulating DataFrames with pandas Course.pdf', title='Manipulating DataFrames with pandas Course', content='''
        In this course, you'll learn how to leverage pandas' extremely powerful data manipulation engine to get the most out of your data. You’ll learn how to drill into the data that really matters by extracting, filtering, and transforming data from DataFrames. The pandas library has many techniques that make this process efficient and intuitive. You will learn how to tidy, rearrange, and restructure your data by pivoting or melting and stacking or unstacking DataFrames. These are all fundamental next steps on the road to becoming a well-rounded data scientist, and you will have the chance to apply all the concepts you learn to real-world datasets.''')
        db.session.add(cert)
        cert = Certs(filename='Merging DataFrames with pandas Course.pdf', title='Merging DataFrames with pandas Course', content='''
        As a data scientist, you'll often find that the data you need is not in a single file. It may be spread across a number of text files, spreadsheets, or databases. You’ll want to be able to import the data you’re interested in as a collection of DataFrames and combine them to answer your central questions. This course is all about the act of combining—or merging—DataFrames, an essential part of any data scientist's toolbox. You'll hone your pandas skills by learning how to organize, reshape, and aggregate multiple datasets to answer your specific questions.''')
        db.session.add(cert)
        cert = Certs(filename='Network Analysis in Python (Part 1).pdf', title='Network Analysis in Python (Part 1)', content='''
        From online social networks such as Facebook and Twitter to transportation networks such as bike sharing systems, networks are everywhere—and knowing how to analyze them will open up a new world of possibilities for you as a data scientist. This course will equip you with the skills to analyze, visualize, and make sense of networks. You'll apply the concepts you learn to real-world network data using the powerful NetworkX library. With the knowledge gained in this course, you'll develop your network thinking skills and be able to look at your data with a fresh perspective.''')
        db.session.add(cert)
        cert = Certs(filename='Pandas Foundations Course.pdf', title='pandas Foundations Course', content='''
        pandas DataFrames are the most widely used in-memory representation of complex data collections within Python. Whether in finance, a scientific field, or data science, familiarity with pandas is essential. This course teaches you to work with real-world datasets containing both string and numeric data, often structured around time series. You will learn powerful analysis, selection, and visualization techniques in this course.''')
        db.session.add(cert)
        cert = Certs(filename='Python Data Science Toolbox (Part 1) Course.pdf', title='Python Data Science Toolbox (Part 1) Course', content='''
        It's time to push forward and develop your Python chops even further. There are tons of fantastic functions in Python and its library ecosystem. However, as a data scientist, you'll constantly need to write your own functions to solve problems that are dictated by your data. You will learn the art of function writing in this first Python Data Science Toolbox course. You'll come out of this course being able to write your very own custom functions, complete with multiple parameters and multiple return values, along with default arguments and variable-length arguments. You'll gain insight into scoping in Python and be able to write lambda functions and handle errors in your function writing practice. And you'll wrap up each chapter by using your new skills to write functions that analyze Twitter DataFrames.''')
        db.session.add(cert)
        cert = Certs(filename='Python Data Science Toolbox (Part 2) Course.pdf', title='Python Data Science Toolbox (Part 2) Course', content='''
        In this second Python Data Science Toolbox course, you'll continue to build your Python data science skills. First, you'll learn about iterators, objects you have already encountered in the context of for loops. You'll then learn about list comprehensions, which are extremely handy tools for all data scientists working in Python. You'll end the course by working through a case study in which you'll apply all the techniques you learned in both parts of this course.''')
        db.session.add(cert)
        cert = Certs(filename='Python Programming Track.pdf', title='Python Programming Track', content='''
        A Python Programmer uses their programming skills to wrangle data and build tools for data analysis.''')
        db.session.add(cert)
        cert = Certs(filename='Statistical Thinking in Python (Part 1).pdf', title='Statistical Thinking in Python (Part 1)', content='''
        After all of the hard work of acquiring data and getting them into a form you can work with, you ultimately want to make clear, succinct conclusions from them. This crucial last step of a data analysis pipeline hinges on the principles of statistical inference. In this course, you will start building the foundation you need to think statistically, speak the language of your data, and understand what your data is telling you. The foundations of statistical thinking took decades to build, but can be grasped much faster today with the help of computers. With the power of Python-based tools, you will rapidly get up-to-speed and begin thinking statistically by the end of this course.''')
        db.session.add(cert)
        cert = Certs(filename='Statistical Thinking in Python (Part 2).pdf', title='Statistical Thinking in Python (Part 2)', content='''
        After completing Statistical Thinking in Python (Part 1), you have the probabilistic mindset and foundational hacker stats skills to dive into data sets and extract useful information from them. In this course, you will do just that, expanding and honing your hacker stats toolbox to perform the two key tasks in statistical inference, parameter estimation and hypothesis testing. You will work with real data sets as you learn, culminating with analysis of measurements of the beaks of the Darwin's famous finches. You will emerge from this course with new knowledge and lots of practice under your belt, ready to attack your own inference problems out in the world.''')
        db.session.add(cert)
        cert = Certs(filename='Supervised Learning with scikit-learn.pdf', title='Supervised Learning with scikit-learn', content='''
        Machine learning is the field that teaches machines and computers to learn from existing data to make predictions on new data: Will a tumor be benign or malignant? Which of your customers will take their business elsewhere? Is a particular email spam? In this course, you'll learn how to use Python to perform supervised learning, an essential component of machine learning. You'll learn how to build predictive models, tune their parameters, and determine how well they will perform with unseen data—all while using real world datasets. You'll be using scikit-learn, one of the most popular and user-friendly machine learning libraries for Python.''')
        db.session.add(cert)
        cert = Certs(filename='Unsupervised Learning in Python.pdf', title='Unsupervised Learning in Python', content='''
        Say you have a collection of customers with a variety of characteristics such as age, location, and financial history, and you wish to discover patterns and sort them into clusters. Or perhaps you have a set of texts, such as wikipedia pages, and you wish to segment them into categories based on their content. This is the world of unsupervised learning, called as such because you are not guiding, or supervising, the pattern discovery by some prediction task, but instead uncovering hidden structure from unlabeled data. Unsupervised learning encompasses a variety of techniques in machine learning, from clustering to dimension reduction to matrix factorization. In this course, you'll learn the fundamentals of unsupervised learning and implement the essential algorithms using scikit-learn and scipy. You will learn how to cluster, transform, visualize, and extract insights from unlabeled datasets, and end the course by building a recommender system to recommend popular musical artists.''')
        db.session.add(cert)
        cert = Certs(filename='Writing Functions and Stored Procedures in SQL.pdf', title='Writing Functions and Stored Procedures in SQL', content='''
        Take your SQL Server programming to the next level. First, we demystify how to manipulate datetime data by performing temporal exploratory data analysis with the Washington DC BikeShare transactional dataset. Then, you’ll master how to create, update, and execute user-defined functions and stored procedures. You will learn the proper context for each modular programming tool and best practices. In the final chapter, you will apply all of your new skills to solve a real-world business case identifying the New York City yellow taxi utilization for each borough, and which pickup locations should be scheduled for each driver shift.''')
        db.session.add(cert)

        cert = Certs(filename='Marketing Analytics Predicting Customer Churn.pdf', title='Marketing Analytics: Predicting Customer Churn in Python', content='''
        Churn is when a customer stops doing business or ends a relationship with a company. It’s a common problem across a variety of industries, from telecommunications to cable TV to SaaS, and a company that can predict churn can take proactive action to retain valuable customers and get ahead of the competition. This course will provide you a roadmap to create your own customer churn models. You’ll learn how to explore and visualize your data, prepare it for modeling, make predictions using machine learning, and communicate important, actionable insights to stakeholders. By the end of the course, you’ll become comfortable using the pandas library for data analysis and the scikit-learn library for machine learning.''')
        db.session.add(cert)
        cert = Certs(filename='Machine Learning for Marketing in Python.pdf', title='Machine Learning for Marketing in Python', content='''
        The rise of machine learning (almost sounds like "rise of the machines"?) and applications of statistical methods to marketing have changed the field forever. Machine learning is being used to optimize customer journeys which maximize their satisfaction and lifetime value. This course will give you the foundational tools which you can immediately apply to improve your company’s marketing strategy. You will learn how to use different techniques to predict customer churn and interpret its drivers, measure, and forecast customer lifetime value, and finally, build customer segments based on their product purchase patterns. You will use customer data from a telecom company to predict churn, construct a recency-frequency-monetary dataset from an online retailer for customer lifetime value prediction, and build customer segments from product purchase data from a grocery shop.''')
        db.session.add(cert)

        cert = Certs(filename='Neural Networks and Deep Learning.pdf', title='Neural Networks and Deep Learning', content='''
        If you want to break into cutting-edge AI, this course will help you do so. Deep learning engineers are highly sought after, and mastering deep learning will give you numerous new career opportunities. Deep learning is also a new "superpower" that will let you build AI systems that just weren't possible a few years ago. ''')
        db.session.add(cert)
        cert = Certs(filename='Introduction to SQL Server.pdf', title='Introduction to SQL Server', content='''
        SQL is an essential skill for data scientists, and Microsoft SQL Server is one of the world's most popular database systems. In this course, you'll start with simple SELECT statements, and refine these queries with ORDER BY and WHERE clauses. You'll learn how to group and aggregate your results, and also how to work with strings. You'll also cover the most common way to join tables, how to create tables, and inserting and updating data. You'll mainly work with the Chinook digital media database, representing sales of various artists and tracks. The music theme continues with the classic rock and Eurovision datasets, while we also look at trends in power outages in the US. Overall, you'll become proficient in the most common data manipulation tasks in SQL Server and build a solid foundation for the upcoming T-SQL courses here on DataCamp! ''')
        db.session.add(cert)


        fpImage = FPCarousel(filename='test12.JPG', title='Borneo - Malaysia 2019', small_text='...', user_id=1)
        db.session.add(fpImage)
        fpImage = FPCarousel(filename='BB1_0014.JPG', title='Borneo - Malaysia 2019', small_text='Orangutan', user_id=1)
        db.session.add(fpImage)
        fpImage = FPCarousel(filename='BB1_2820.JPG', title='Brazil - Pantanal 2020', small_text='Arara', user_id=1)
        db.session.add(fpImage)
        fpImage = FPCarousel(filename='BB1_3961.JPG', title='Brazil - Pantanal 2020', small_text='Capybara', user_id=1)
        db.session.add(fpImage)

        # tag types - bootstrap classes
        # https://getbootstrap.com/docs/4.1/components/badge/
        # 1
        tagType = TagTypes(tagType='badge-primary')
        db.session.add(tagType)
        # 2
        tagType = TagTypes(tagType='badge-secondary')
        db.session.add(tagType)
        # 3
        tagType = TagTypes(tagType='badge-success')
        db.session.add(tagType)
        # 4
        tagType = TagTypes(tagType='badge-danger')
        db.session.add(tagType)
        # 5
        tagType = TagTypes(tagType='badge-warning')
        db.session.add(tagType)
        # 6
        tagType = TagTypes(tagType='badge-info')
        db.session.add(tagType)
        # 7
        tagType = TagTypes(tagType='badge-light')
        db.session.add(tagType)
        # 8
        tagType = TagTypes(tagType='badge-dark')
        db.session.add(tagType)
        
        tag = PostTags(tagName='Nature', tagType=3)
        db.session.add(tag)
        tag = PostTags(tagName='Tech', tagType=8)
        db.session.add(tag)
        tag = PostTags(tagName='Python', tagType=5)
        db.session.add(tag)

        apiPost = APIPosted(name='Dave Fenwick', msg='Example Toast Message')
        db.session.add(apiPost)

        holtag = HolidayTags(tagName='Singapore')
        db.session.add(holtag)
        holtag = HolidayTags(tagName='Borneo')
        db.session.add(holtag)
        holtag = HolidayTags(tagName='Sepilok')
        db.session.add(holtag)
        holtag = HolidayTags(tagName='Orangutan')
        db.session.add(holtag)

        db.session.commit()
        return 'Success!'

def main():
    siteDBFile = 'DFWeb/site.db'
    if exists(siteDBFile):
        print('Database Already Exists')
        delete = input('Would you like to delete the DB? (y/n)')
        if delete.lower() == 'y':
            remove(siteDBFile)
            print('Previous DB Deleted')
        else: return
    db.create_all(app=create_app())
    print('Database created')
    print('Adding data to DB...')
    addDataToDB()

if __name__ == '__main__':
    sys.exit(main())