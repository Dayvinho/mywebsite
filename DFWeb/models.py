from DFWeb import login_manager, db
from flask_login import UserMixin
from flask import current_app
from datetime import datetime
from os.path import join
import os

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

class User(db.Model, UserMixin):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(25), nullable=False, unique=True)
    email = db.Column(db.String(100), nullable=False, unique=True)
    password = db.Column(db.String(100), nullable=False)
    profilePic = db.Column(db.String(20), nullable=False, default='default.jpg')
    posts = db.relationship('Post', backref='author', lazy='dynamic')
    fpImgs = db.relationship('FPCarousel', backref='fpImgs', lazy='dynamic')
    holImgs = db.relationship('HolidayImgs', backref='holImgs', lazy='dynamic')

    def is_active(self):
        """True, as all users are active."""
        return True

    def get_id(self):
        """Return the id to satisfy Flask-Login's requirements."""
        return self.id

    def is_anonymous(self):
        """False, as anonymous users aren't supported."""
        return False

    def is_authenticated(self):
        if current_user.is_authenticated:
            return True
        else: 
            return False

class Certs(db.Model):
    __tablename__ = 'certs'
    id = db.Column(db.Integer, primary_key=True)
    filename = db.Column(db.String(100), nullable=False)
    title = db.Column(db.String(100), nullable=False)
    content = db.Column(db.Text, nullable=False)

class Post(db.Model):
    __tablename__ = 'post'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    date_posted = db.Column(db.DateTime, nullable=False, default=datetime.utcnow, 
                            index=True)
    content = db.Column(db.Text, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    postTagsR = db.relationship('PostTagsPosts', backref='tags', lazy='dynamic')
    
    @staticmethod
    def deletePost(postID):
        Post.query.filter(Post.id == postID).delete()
        db.session.commit()

# to be used to assign colours/types to tags
class TagTypes(db.Model):
    __tablename__ = 'tagTypes'
    id = db.Column(db.Integer, primary_key=True)
    tagType = db.Column(db.String(50), nullable=False)

class PostTags(db.Model):
    __tablename__ = 'postTags'
    id = db.Column(db.Integer, primary_key=True)
    tagName = db.Column(db.String(30), unique=True, nullable=False)
    tagType = db.Column(db.Integer, db.ForeignKey('tagTypes.id'), nullable=False)

class PostTagsPosts(db.Model):
    __tablename__ = 'postTagsPosts'
    id = db.Column(db.Integer, primary_key=True)
    post_id = db.Column(db.Integer, db.ForeignKey('post.id'), nullable=False)
    tag_id = db.Column(db.Integer, db.ForeignKey('postTags.id'), nullable=False)

class FPCarousel(db.Model):
    __tablename__ = 'fpCarousel'
    id = db.Column(db.Integer, primary_key=True)
    filename = db.Column(db.String(100), nullable=False, unique=True)
    title = db.Column(db.String(50), nullable=False)
    small_text = db.Column(db.String(20))
    date_posted = db.Column(db.DateTime, default=datetime.utcnow, index=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

    @staticmethod
    def deleteImg(imgID):
        filename = FPCarousel.query.filter(FPCarousel.id == imgID).first()
        filePath = join(current_app.config['CAROUSEL_DIR'], filename.filename)
        os.remove(filePath)
        FPCarousel.query.filter(FPCarousel.id == filename.id).delete()
        db.session.commit()

class HolidayImgs(db.Model):
    __tablename__ = 'holidayImgs'
    id = db.Column(db.Integer, primary_key=True)
    filename = db.Column(db.String(100), nullable=False, unique=True)
    title = db.Column(db.String(50), nullable=False)
    small_text = db.Column(db.String(20))
    #date_taken = db.Column(db.DateTime, index=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    holTags = db.relationship('HolidayTagsImgs', backref='tags', lazy='dynamic')

    @staticmethod
    def deleteImg(imgID):
        HolidayImgs.query.filter(HolidayImgs.id == imgID).delete()
        db.session.commit()

class HolidayTags(db.Model):
    __tablename__ = 'holidayTags'
    id = db.Column(db.Integer, primary_key=True)
    tagName = db.Column(db.String(30), unique=True, nullable=False)
    holImgs = db.relationship('HolidayTagsImgs', backref='imgs', lazy='dynamic')

class HolidayTagsImgs(db.Model):
    __tablename__ = 'holidayTagsImgs'
    id = db.Column(db.Integer, primary_key=True)
    img_id = db.Column(db.Integer, db.ForeignKey('holidayImgs.id'), nullable=False)
    tag_id = db.Column(db.Integer, db.ForeignKey('holidayTags.id'), nullable=False)

class APIPosted(db.Model):
    __tablename__ = 'apiPosted'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    msg = db.Column(db.String(250), nullable=False)
    date_posted = db.Column(db.DateTime, default=datetime.utcnow, index=True)

