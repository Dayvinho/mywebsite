import binascii
import os
from os.path import join, dirname, abspath

class Config:
    SECRET_KEY = binascii.hexlify(os.urandom(24))
    # Some directories
    ROOT_DIR = dirname(abspath(__file__))
    CERTS_DIR = join(ROOT_DIR, 'static', 'certs')
    CAROUSEL_DIR = join(ROOT_DIR, 'static', 'pics', 'carousel')
    PROFILE_DIR = join(ROOT_DIR, 'static', 'pics', 'profiles')
    # SQL Alchemy
    SQLALCHEMY_DATABASE_URI = 'sqlite:///site.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # Google analytics keypath
    KEY_FILEPATH = join(ROOT_DIR, 'views', 'projects', 'keys', 'personalwebsite-258816-ad4451091aff.json')
    # Avocado data filepath
    AVOCADO_DATA = join(ROOT_DIR, 'static', 'data', 'avocado.csv')
    # Set MAX Image Dimensions For Carousel
    CAROUSEL_MAX_WIDTH = 1920
    CAROUSEL_MAX_HEIGHT = 1080
    # Example PDF
    EXAMPLE_PDF = join(ROOT_DIR, 'static', 'data', 'sample.pdf')
    # Iris Data filepath
    IRIS_DATA = join(ROOT_DIR, 'static', 'data', 'iris.txt')