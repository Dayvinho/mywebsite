from flask import Flask
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from DFWeb.config import Config
from datetime import datetime

db = SQLAlchemy()
bcrypt = Bcrypt()
login_manager = LoginManager()
#login_manager.login_view = 'admin.login'
login_manager.login_message_category = 'info'

# jinja2 functions
def formatTime(time):
    return datetime.utcfromtimestamp(time)

def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(Config)

    db.init_app(app)
    bcrypt.init_app(app)
    login_manager.init_app(app)

    from DFWeb.views.main.routes import main
    from DFWeb.views.projects.routes import projects
    from DFWeb.views.admin.routes import admin
    from DFWeb.views.posts.routes import posts
    from DFWeb.views.errors.handlers import errors

    app.register_blueprint(main)
    app.register_blueprint(projects)
    app.register_blueprint(admin)
    app.register_blueprint(posts)
    app.register_blueprint(errors)

    app.jinja_env.globals['formatTime'] = formatTime

    return app