from flask import (render_template, url_for, flash, redirect, 
                   request, Blueprint, current_app)
from datetime import datetime
from dateutil.relativedelta import relativedelta
from os import listdir
from DFWeb.models import FPCarousel

main = Blueprint('main', __name__)

@main.route("/")
@main.route('/home', methods=['GET'])
def home():
    dtNow = datetime.now()
    bd = datetime(1988, 8, 13)
    sd = datetime(2015, 5, 27)
    age = relativedelta(dtNow, bd).years
    worklength = relativedelta(dtNow, sd).years
    carouselImages = FPCarousel.query.all()
    kwargs = {
        'age': age,
        'worklength': worklength,
        'carouselImages': carouselImages
    }
    return render_template('home.html', **kwargs)