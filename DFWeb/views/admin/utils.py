from PIL import Image
from os.path import splitext, join
from werkzeug.utils import secure_filename
from flask import current_app
from DFWeb.models import PostTags, TagTypes

# save uploaded images
def saveImg(imgData, saveDir, resizeImg=False):
    filename = secure_filename(imgData.filename)
    filePath = join(current_app.config[saveDir],  filename)
    fType = filePath.split('.')[-1]
    if resizeImg:
        resize(filePath, fType)
    else:
        imgData.save(filePath)
    return filename


# resize uploaded images
def resize(filePath, fType):
    print('RESIZING!!!')
    im = Image.open(filePath)
    f, e = splitext(filePath)
    maxwidth = current_app.config['CAROUSEL_MAX_WIDTH']    
    wpercent = (maxwidth/float(im.size[0]))
    hsize = int((float(im.size[1])*float(wpercent)))
    newSize = (maxwidth, hsize)
    imResize = im.resize(newSize, Image.ANTIALIAS)
    if fType.lower() == 'png':
        imResize.save(f'{f}.png', 'PNG', quality=90)
    elif fType.lower() == 'jpg':
        imResize.save(f'{f}.jpg', 'JPEG', quality=90)


def saveFile(fileData, saveDir):
    filename = secure_filename(fileData.filename)
    filePath = join(current_app.config[saveDir],  filename)
    fileData.save(filePath)
    return filename


def getAllTags():
    return PostTags.query.join(TagTypes, TagTypes.id == PostTags.tagType)\
        .add_columns(PostTags.tagName, TagTypes.tagType).all()