from flask import (render_template, Blueprint, current_app, redirect, url_for,
                    flash, request)
from flask_login import login_user, current_user, logout_user, login_required
from DFWeb.models import User, FPCarousel, PostTags, TagTypes, Certs
from DFWeb import db, bcrypt
from DFWeb.views.admin.forms import (LoginForm, CarouselDeleteForm, 
                                    CarouselUpdateForm, UpdateProfileForm,
                                    UpdatePostTagsForm, AddNewAdminForm,
                                    AddNewCertificateForm, DeletePostTagsForm)
from DFWeb.views.admin.utils import saveImg, saveFile, getAllTags

admin = Blueprint('admin', __name__)


@admin.route('/admin/login', methods=['GET', 'POST'])
def adminLogin():
    if current_user.is_authenticated:
        return redirect(url_for('main.home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            next_page = request.args.get('next')
            flash('You Are Now Logged In', 'success')
            return redirect(next_page) if next_page else redirect(url_for('main.home'))
        else:
            flash('Login Unsuccessful, Please Check Email and Password', 'danger')
    kwargs = {
        'form': form
    }
    return render_template('admin/adminLogin.html', **kwargs)


@admin.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('main.home'))


@admin.route('/admin/update/carousel', methods=['GET', 'POST'])
@login_required
def updateCarousel():
    form = CarouselUpdateForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            try:
                filename = saveImg(form.photo.data, 'CAROUSEL_DIR', 
                                    resizeImg=True)
                
                carouselItem = FPCarousel(filename=filename, 
                                        title=form.title.data, 
                                        small_text=form.small_text.data \
                                            if form.small_text.data != '' else '...', 
                                        user_id=current_user.id)
                db.session.add(carouselItem)
            except Exception as e:
                flash(f'Image Cannot Be Added {e}', 'danger')
                return redirect(url_for('admin.updateCarousel'))
            try:
                db.session.commit()
                flash('Image Added', 'success')
            except Exception as e:
                flash(f'Image Not Added', 'danger')
                return redirect(url_for('admin.updateCarousel'))
            return redirect(url_for('admin.updateCarousel'))
   
    kwargs = {
        'form': form,
    }
    return render_template('admin/updateCarousel.html', **kwargs)


@admin.route('/admin/delete/carousel', methods=['GET', 'POST'])
@login_required
def deleteCarousel():
    form = CarouselDeleteForm()
    fpCarouselItems = FPCarousel.query.all()
    if request.method == 'POST':
        if form.validate_on_submit():
            imgVals = request.form.getlist('checkSelect')
            for imgID in imgVals:
                FPCarousel.deleteImg(imgID)
            flash('Image Deleted', 'success')
            # Image still needs to be deleted from folder
            return redirect(url_for('admin.deleteCarousel'))
   
    kwargs = {
        'form': form,
        'fpCarouselItems': fpCarouselItems
    }
    return render_template('admin/deleteCarousel.html', **kwargs)


@admin.route('/admin/update/profile', methods=['GET', 'POST'])
@login_required
def updateProfile():
    form = UpdateProfileForm()
    userData = User.query.filter_by(id=current_user.id).first()
    if request.method == 'POST':
        if form.validate_on_submit():
            if form.username.data:
                current_user.username = form.username.data
            if form.email.data:
                current_user.email = form.email.data
            if form.profilePic.data:
                filename = saveImg(form.profilePic.data, 'PROFILE_DIR')
                current_user.profilePic = filename
            db.session.commit()
            flash('Your account has been updated!', 'success')
            return redirect(url_for('admin.updateProfile'))      
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.email.data = current_user.email
    kwargs = {
        'form': form,
        'userData': userData
    }
    return render_template('admin/updateProfile.html', **kwargs)


@admin.route('/admin/addNewPostTag', methods=['GET', 'POST'])
#@login_required
def addNewPostTags():
    tagTypes = TagTypes.query.all()
    tags = [(tag.id, str(tag.tagType).replace('badge-','').title()) for tag in tagTypes]
    form = UpdatePostTagsForm()
    form.tagType.choices = tags
    allTags = getAllTags()
    if request.method == 'GET':
        kwargs = {
            'form': form,
            'allTags': allTags,
            'tagTypes': tagTypes
        }
        return render_template('admin/addNewPostTags.html', **kwargs)
    elif request.method == 'POST':
        if form.validate_on_submit():
            newTag = PostTags(tagName=form.tagName.data,
                              tagType=form.tagType.data)
            db.session.add(newTag)
            db.session.commit()
            return redirect(url_for('admin.addNewPostTags'))


@admin.route('/admin/delete/postTags', methods=['GET', 'POST'])
@login_required
def deletePostTags():
    form = DeletePostTagsForm()
    allTags = getAllTags()
    print(allTags)
    for tag in allTags:
        print(tag.tagName)
    if request.method == 'GET':
        kwargs = {
            'form': form,
            'allTags': allTags

        }
        return render_template('admin/deletePostTags.html', **kwargs)


@admin.route('/admin/addNewAdmin', methods=['GET', 'POST'])
@login_required
def addAdminUser():
    form = AddNewAdminForm()
    return render_template('admin/addNewAdmin.html', **kwargs)
    

@admin.route('/admin/addNewCert', methods=['GET', 'POST'])
@login_required
def addCertificate():
    form = AddNewCertificateForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            filename = saveFile(form.filename.data, 'CERTS_DIR')
            cert = Certs(filename = filename,
                         title = form.title.data,
                         content = form.content.data)
            db.session.add(cert)
            db.session.commit()
            return redirect(url_for('admin.addCertificate'))
    kwargs = {
        'form': form
    }
    return render_template('admin/addNewCertificate.html', **kwargs)