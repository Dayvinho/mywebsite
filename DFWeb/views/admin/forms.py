from flask_wtf import FlaskForm
from wtforms import (StringField, PasswordField, SubmitField, BooleanField,
                    FormField, SelectField)
from flask_wtf.file import FileField, FileRequired
from wtforms.validators import (DataRequired, Length, Email, ValidationError, 
                                EqualTo)
from flask_login import current_user
from DFWeb.models import User


class AddNewAdminForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    confirmPassword = PasswordField('Confirm Password', 
                                        validators=[DataRequired()])
    submit = SubmitField('Create')

    def validateEmail(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user:
            raise ValidationError('This user already exists!')


class UpdateAdminForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    username = StringField('Username', validators=[DataRequired()])
    
    def validateUsername(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user:
            raise ValidationError('That username is taken. Please choose a different one.')
    
    def validateEmail(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user:
            raise ValidationError('That email is taken. Please choose a different one.')


class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember =  BooleanField('Remember me?')
    submit = SubmitField('Log In')


class CarouselUpdateForm(FlaskForm):
    photo = FileField(validators=[FileRequired()])
    title = StringField('Image Title', validators=[DataRequired()])
    small_text = StringField('Image Small Text')
    submit = SubmitField('Add Image')


class CarouselDeleteForm(FlaskForm):
    delete = SubmitField('Delete')


class UpdateProfileForm(FlaskForm):
    username = StringField('Username')
    email = StringField('Email')
    profilePic = FileField()
    submit = SubmitField('Confirm Changes')


class UpdatePasswordForm(FlaskForm):
    password = PasswordField('Password', validators=[DataRequired()])
    confPassword = PasswordField('Confirm Password', 
                                validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Confirm')


class UpdatePostTagsForm(FlaskForm):
    tagName = StringField('Tag Name', validators=[DataRequired()])
    tagType = SelectField('Tag Type', coerce=int)
    submit = SubmitField('Confirm')


class DeletePostTagsForm(FlaskForm):
    delete = SubmitField('Delete')


class AddNewCertificateForm(FlaskForm):
    filename = FileField(validators=[FileRequired()])
    title = StringField('Title', validators=[DataRequired()])
    content = StringField('Course Description', validators=[DataRequired()])
    submit = SubmitField('Confirm')
