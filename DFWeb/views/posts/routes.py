from flask import (render_template, url_for, flash, redirect,
                   request, abort, Blueprint, current_app)
from flask_login import login_required, current_user
from DFWeb import db
from DFWeb.models import Post, PostTagsPosts, PostTags, TagTypes
from DFWeb.views.posts.forms import PostForm, PostAddTagsForm
from DFWeb.views.posts.utils import (getPosts, getPost, getTags, 
                                    getTagDetails, getPostsByTag)

posts = Blueprint('posts', __name__)

@posts.route('/posts/new', methods=['GET', 'POST'])
@login_required
def newPost():
    form = PostForm()
    if form.validate_on_submit():
        post = Post(title=form.title.data, 
                    content=request.form.get('editordata'), 
                    user_id=current_user.id)
        db.session.add(post)
        db.session.flush()
        thisPostID = post.id
        db.session.commit()
        return redirect(url_for('posts.addPostTags', thisPostID=thisPostID))
        #flash('Post has been created!', 'success')
        #return redirect(url_for('main.home'))
    kwargs = {
        'form': form
    }
    return render_template('posts/newPost.html', **kwargs)

@posts.route('/posts/addPostTags', methods=['GET', 'POST'])
def addPostTags():
    form = PostAddTagsForm()
    thisPostID = request.args['thisPostID']
    if form.validate_on_submit():
        tags = form.tags.data
        if tags:
            for tag in tags:
                postTags = PostTagsPosts(post_id=thisPostID, tag_id=tag)
                db.session.add(postTags)
            db.session.commit()
        return redirect(url_for('posts.viewPosts'))
    kwargs = {
        'form': form
    }
    return render_template('posts/addPostTags.html', **kwargs)

@posts.route('/posts/viewall', methods=['GET'])
def viewPosts():
    page = request.args.get('page', 1, type=int)
    allPostsData = getPosts(page)
    kwargs = {
        'allPosts': allPostsData['posts'],
        'postTags': allPostsData['postTags']
    }
    return render_template('posts/viewPosts.html', **kwargs)

@posts.route('/posts/view/<postID>', methods=['GET'])
def viewPost(postID):
    post = getPost(postID)
    postTagsIDs = [tag.tag_id for tag in post.postTagsR.all()]
    tagsForPost = getTagDetails(postTagsIDs)
    kwargs = {
        'post': post,
        'tagsForPost': tagsForPost
    }
    return render_template('posts/viewPost.html', **kwargs)

@posts.route('/posts/viewByTag/<tagName>')
def viewPostsByTag(tagName):
    print(tagName)
    page = request.args.get('page', 1, type=int)
    taggedPosts = getPostsByTag(tagName, page)
    kwargs = {
        'allPosts': taggedPosts['posts'],
        'postTags': taggedPosts['postTags'],
        'tagName': tagName
    }
    return render_template('posts/viewPostsByTags.html', **kwargs)