from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField, SelectMultipleField
from wtforms.validators import DataRequired
from DFWeb.models import PostTags

class PostForm(FlaskForm):
    title = StringField('Title', validators=[DataRequired()])
    submit = SubmitField('Post')

class PostAddTagsForm(FlaskForm):
    tags = SelectMultipleField('Select Post Tag(s)', coerce=int, validators=[DataRequired()])
    submit = SubmitField('Add Tags To Post')

    def __init__(self, *args, **kwargs):
        super(PostAddTagsForm, self).__init__(*args, **kwargs)
        self.tags.choices = [(tag.id, tag.tagName) for tag in PostTags.query.all()]

