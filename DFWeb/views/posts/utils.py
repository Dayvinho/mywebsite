from DFWeb.models import Post, PostTagsPosts, PostTags, TagTypes
from flask import current_app
from DFWeb import db

def getPosts(page):
    return {
        'posts': Post.query.order_by(Post.date_posted.desc())\
                    .paginate(page=page, per_page=3),
        'postTags': db.session.query(PostTagsPosts, PostTags, TagTypes)\
                    .join(PostTags, PostTagsPosts.tag_id==PostTags.id)\
                    .join(TagTypes, PostTags.tagType==TagTypes.id).all()
    }
    
def getPost(postID):
    return Post.query.filter_by(id=postID).first()

def getTags(postID):
    return PostTagsPosts.query.filter_by(post_id=postID).all()

def getTagDetails(tags):
    return [tag for tag in db.session.query(PostTags, TagTypes).join(TagTypes)\
        .filter(PostTags.id.in_(tuple(tags))).all()]

def getPostsByTag(tag, page):
    return {
        'posts': db.session.query(Post, PostTagsPosts, PostTags, TagTypes)\
                    .join(PostTagsPosts, Post.id==PostTagsPosts.post_id)\
                    .join(PostTags, PostTagsPosts.tag_id==PostTags.id)\
                    .join(TagTypes, PostTags.tagType==TagTypes.id)\
                    .filter(PostTags.tagName==tag)\
                    .order_by(Post.date_posted.desc())\
                    .paginate(page=page, per_page=3),
        'postTags': db.session.query(PostTagsPosts, PostTags, TagTypes)\
                    .join(PostTags, PostTagsPosts.tag_id==PostTags.id)\
                    .join(TagTypes, PostTags.tagType==TagTypes.id).all()
    }
            