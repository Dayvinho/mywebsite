from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, RadioField, IntegerField
from wtforms.validators import DataRequired, NumberRange
#from wtforms.widgets import TableWidget

class AlliantsDevTest(FlaskForm):
    searchTerm = StringField('Enter Search Term Please', validators=[DataRequired()])
    vidLength = RadioField('Select Video Length',
                           choices=[('Any', 'Video of any length'),
                           ('Short', 'Short videos'),('Medium', 'Medium videos'),
                           ('Long', 'Long videos')])
    numOfResults = IntegerField('Number of results (1-20)', validators=[DataRequired(),
                                NumberRange(min=1,max=20,
                                message='Please select value between 1 - 20')])
    submit = SubmitField('Search')
