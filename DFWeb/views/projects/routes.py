import json
# Problems with PyPDF2 in Ubuntu Server 19.1 and Pythonanywhere.com
#import PyPDF2
import random
from os.path import join
from datetime import datetime
from flask import (render_template, Blueprint, make_response, current_app,
                    request, flash, redirect, url_for, jsonify)
from DFWeb.views.projects.utils import (getCerts, getUserData, getUserWeather, 
                                        getAccessToken, createForecastChart,
                                        getAvocadoData, getIrisData, 
                                        getYoutubeVids, getFlikrImgs,
                                        keyCheck)
from DFWeb.models import APIPosted
from DFWeb.views.projects.forms import AlliantsDevTest
from flask_paginate import Pagination, get_page_parameter, get_page_args


projects = Blueprint('projects', __name__)


# datacamp certicates display
@projects.route('/projects/certs')
def certsDisplay():
    page = request.args.get('page', 1, type=int)
    certs = getCerts(page)
    kwargs = {
        'certs': certs
    }
    return render_template('projects/certs.html', **kwargs)


# individual cert display
@projects.route('/projects/certs/<certName>')
def singleCertDisplay(certName):
    pdf_file = join(current_app.config['CERTS_DIR'], certName)

    with open(pdf_file, 'rb') as pdf:
        binary_pdf = pdf.read()
        response = make_response(binary_pdf)
        response.headers['Content-Type'] = 'application/pdf'
        response.headers['Content-Disposition'] = f'inline; filename={pdf_file}'
        return response


# api tool to get user location based on IP (not very reliable I know)
# and display user weather information
@projects.route('/projects/apiTools/weatherAPI')
def weatherAPI():
    try:
        #userIP = request.headers['X-Real-IP']
        userIP = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)  
    except:
        userIP = request.remote_addr
        flash('Problem Obtaining User IP Adress', 'warning')

    userData = getUserData(userIP)
    weatherData = getUserWeather(userData['city'], userData['country_code'])
    forecastDataPre = getUserWeather(userData['city'], userData['country_code'], 
                                    weather='forecast')
    forecastData = forecastDataPre['list']
    renderChart = createForecastChart(forecastData)

    kwargs = {
        'userData': userData,
        'weatherData': weatherData,
        'renderChart': renderChart
    }
    return render_template('projects/apiWeather.html', **kwargs)


# Display myWebsite data from Google Analytics
@projects.route('/projects/googleAnalytics')
def GAPublicView():
    kwargs = {'ACCESS_TOKEN_FROM_SERVICE_ACCOUNT': getAccessToken()}
    return render_template('projects/googleAnalytics.html', **kwargs)


# Display data from Kaggle in some graphs etc
@projects.route('/projects/dataVis')
def dataDisplay():
    avoData = getAvocadoData()
    kwargs = {
        'avoData': avoData
    }
    return render_template('projects/dataVis.html', **kwargs)


# Display Toast notifications that have been POSTed to website using JSON
@projects.route('/projects/public/api/postAndDisplay', methods=['POST', 'GET'])
def publicAPIInput():
    if request.method == 'POST':
        responce = request.get_json()
        apiData = APIPosted(name=responce['data']['name'],
                            msg=responce['data']['message'])
        db.session.add(apiData)
        db.session.commit()
        return '', 200
        #return redirect(url_for('projects.publicAPIInput'))
    elif request.method == 'GET':
        apiData = APIPosted.query.all()
        kwargs = {
            'apiData': apiData
        }
        return render_template('projects/apiReceive.html', **kwargs)


# Read data from PDF example
@projects.route('/projects/readPDF')
def readPDF():
    # BELOW NOT WORKING IN UBUNTU SERVER OR PYTHON ANYWHERE :(
    #pageData = []
    #with open(current_app.config['EXAMPLE_PDF'], 'rb') as f:
    #    fileReader = PyPDF2.PdfFileReader(f)
    #    numP = fileReader.numPages
    #    for x in range(numP):
    #        page = fileReader.getPage(x)
    #        pageData.append(page.extractText())
    #kwargs = {
    #    'numP': numP,
    #    'pageData': pageData
    #}
    #return render_template('projects/readPDFs.html', **kwargs)
    return render_template('projects/readPDFs.html')


# Read data from PDF example
@projects.route('/projects/writePDF')
def writePDF():
    return render_template('projects/writePDFs.html')


# information about this website and link to git repo
@projects.route('/projects/thisWebsite')
def thisWebsite():
    return render_template('projects/thisWebsite.html')


# work in progress
@projects.route('/projects/ML/irisData')
def displayIrisData():
    irisData = getIrisData()
    kwargs = {
        'irisData': irisData
    }
    return render_template('projects/ML/irisData.html', **kwargs)


# retrieve youtube videos and flikr images
@projects.route('/projects/youtubeFlikr', methods=['GET', 'POST'])
def youtubeFlikr(page=1):
    form = AlliantsDevTest()
    if request.method == 'POST':
        if form.validate_on_submit():
            searchTerm = form.searchTerm.data
            vidLength = str(form.vidLength.data).lower()
            numOfResults = form.numOfResults.data
            youtube = getYoutubeVids(searchTerm,
                                     numOfResults=numOfResults,
                                     vidLength=vidLength)
            flikr = getFlikrImgs(searchTerm)

            #youtubeVids = [vid['id']['videoId'] for vid in youtube['items']]
            #flikrImgs = [img['description'] for img in flikr['items']]
            #flikrImgs = flikrImgs[:numOfResults]
            kwargs = {
                'form': form,
                'youtube': youtube,
                'flikr': flikr
            }
            return render_template('projects/youtubeFlikrAPI.html', **kwargs)
    kwargs = {
        'form': form
    }
    return render_template('projects/youtubeFlikrAPI.html', **kwargs)


# youtube and flikr API
@projects.route('/projects/public/api/ytFlikr', methods=['POST'])
def youtubeFlikrAPI():
    if request.method == 'POST':
        try:
            key = request.headers['api-key']
        except Exception:
            response = jsonify({
                'message': 'API Endpoint for Youtube and Flikr API Services'
                })
            return response, 400
        if keyCheck(key) is True:
            query = request.get_json()
            searchTerm = query['data']['searchTerm']
            youtube = getYoutubeVids(searchTerm)
            flikrTemp = getFlikrImgs(searchTerm)
            flikr = random.choices(flikrTemp['items'], k=3)
            flikrImgs = [img['link'] for img in flikr]
            vidID = youtube['items'][0]['id']['videoId']
            youtubeVideoURL = f'https://www.youtube.com/watch?v={vidID}'
            return jsonify(youtube=youtubeVideoURL, flikr=flikrImgs)
        else:
            response = jsonify({'message':'Failed To Authenticate User'})
            return response, 401


@projects.route('/projects/jQuery/api/ytFlikr', methods=['POST'])
def youtubeFlikrAPI_jQuery():
    if request.method == 'POST':
        query = request.get_json()
        searchTerm = query['data']['searchTerm']
        youtube = getYoutubeVids(searchTerm) 
        flikr = getFlikrImgs(searchTerm)
        return jsonify(youtube=youtube, flikr=flikr)
    else:
        message = {'message': 'Failed To Process Request.'}
        return jsonify(message)


@projects.route('/projects/jQuery/ytFlikr', methods=['GET'])
def jQueryYoutubeFlikr():
    return render_template('projects/youtubeFlikr.html')


@projects.route('/projects/jQuery/basicExample', methods=['GET'])
def jQueryExample():
    return render_template('projects/examplejQuery.html')