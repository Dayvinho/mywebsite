import requests
import pygal
import csv
import json
import pandas as pd
import requests
from pandas.io.json import json_normalize
from flask import current_app, jsonify
from DFWeb import db
from DFWeb.models import Certs
from os.path import abspath, dirname, join
from configparser import ConfigParser
from oauth2client.service_account import ServiceAccountCredentials
from itertools import chain
from googleapiclient.discovery import build
from json import loads


conf = ConfigParser()
root = dirname(abspath(__file__))
iniPath = join(root, 'config.ini')
conf.read(iniPath)
ipStackKey = conf['APIKEYS']['ipStackKey']
openWeatherKey = conf['APIKEYS']['openWeatherKey']


def getCerts(page):
    with current_app.app_context():
        return Certs.query.order_by('filename').paginate(page, 5, False)


def getUserData(userIP):
    apiURL = f'http://api.ipstack.com/{userIP}?access_key={ipStackKey}&security=1'
    r = requests.get(apiURL)
    return r.json()


def checkCity(city, countryCode):
    '''
    Some IP Addresses Return None:\n
    This function is a work around for this,
    internal testing for example will return None.
    '''
    if city is None:
        return 'Southampton', 'UK'
    else:
        return city, countryCode


def getUserWeather(city, countryCode, weather='weather'):
    city, countryCode = checkCity(city, countryCode)
    apiURL = f'http://api.openweathermap.org/data/2.5/{weather}?q={city},{countryCode}&appid={openWeatherKey}&units=metric'
    r = requests.get(apiURL)
    return r.json()


def getAccessToken():
    SCOPE = 'https://www.googleapis.com/auth/analytics.readonly'
    return ServiceAccountCredentials.from_json_keyfile_name(
        current_app.config['KEY_FILEPATH'], SCOPE).get_access_token().access_token


def createForecastChart(forecastData):
    df = json_normalize(forecastData)
    df2 = pd.DataFrame(list(chain.from_iterable(df['weather'])))
    dfData = pd.DataFrame.join(df,df2).drop('weather', axis=1)
        
    lineChart = pygal.Line(x_label_rotation=45)
    lineChart.title = 'Weather Forecast (5 Days/3 Hours)'
    lineChart.x_labels = map(str, dfData['dt_txt'])
    lineChart.add('Temp', df['main.temp'])
    lineChart.add('Temp Min', df['main.temp_min'])
    lineChart.add('Temp Max', df['main.temp_max'])
    return lineChart.render_data_uri()


def transoformAvocadoData(filePath):
    '''
    One use function to transform some data
    '''
    df = pd.read_csv(filePath)
    df = df.drop(['Unnamed: 0'], axis=1)
    columns = df.keys()
    returnArray = '{"data":['
    for _, row in df.iterrows():
        returnArray = (f'{returnArray}["{row[columns[0]]}","{row[columns[1]]}","{row[columns[2]]}","{row[columns[3]]}","{row[columns[4]]}","{row[columns[5]]}","{row[columns[6]]}","{row[columns[7]]}","{row[columns[8]]}","{row[columns[9]]}","{row[columns[10]]}","{row[columns[11]]}","{row[columns[12]]}"],')
    returnArray = returnArray[:-1]
    returnArray = f'{returnArray}]}}'
    #data = df.to_json(orient='records')
    return returnArray


def getAvocadoData():
    sumFunc = lambda x : x.astype(int).sum()
    splitFunc = lambda x: x.split('-')[1]
    # load the data into the dataframe
    df = pd.read_csv(current_app.config['AVOCADO_DATA'])
    df['Total$'] = df['Total Volume'] * df['AveragePrice']
    
    preAggedRegions = ['TotalUS', 'Southeast', 'Northeast', 'Midsouth', 'SouthCentral', 'West']
    #dfG = df[~df.region.isin(preAggedRegions)]
    # df for pre-aggregated rows
    #dfA = df[df.region.isin(preAggedRegions)]
    
    # actual avg for all data
    avgPrice = round(df['Total$'].sum()/df['Total Volume'].sum(),2)
    organicAP = round(df['Total$'].loc[df.type == 'organic'].sum()/df['Total Volume'].loc[df.type == 'organic'].sum(),2)
    convAP = round(df['Total$'].loc[df.type == 'conventional'].sum()/df['Total Volume'].loc[df.type == 'conventional'].sum(),2)

    # create box plot of IQR for avocado average price
    boxPlot = pygal.Box()
    boxPlot.title = f'US Avocado Monthly Average Price $ - IQR - {df["Date"].min()} to {df["Date"].max()}'
    boxPlot.add('Average Price', df['AveragePrice'])
    boxPlot.add('Organic AP', df['AveragePrice'].loc[df.type == 'organic'])
    boxPlot.add('Conventional AP', df['AveragePrice'].loc[df.type == 'conventional'])
    boxPlotR = boxPlot.render_data_uri()

    # creating actual top level average from granular rows
    dfActualAVG = df[['Date','Total Volume', 'Total$']]
    dfActualAVG = dfActualAVG.groupby(['Date'])['Total Volume','Total$'].apply(
        sumFunc).sort_values(by=['Date']).reset_index()
    dfActualAVG['NationalAVG'] = round(dfActualAVG['Total$']/dfActualAVG['Total Volume'],2)
    # getting total us average from dfA

    # getting Type averages
    dfTypeAVG = df[['Date','Total Volume','type', 'Total$']]
    dfTypeAVG = dfTypeAVG.groupby(['Date','type'])['Total Volume','Total$'].apply(
        sumFunc).sort_values(by=['Date']).reset_index()
    dfTypeAVG['NationalAVG'] = round(dfTypeAVG['Total$']/dfTypeAVG['Total Volume'],2) 

    # plotting line chart of national average price, by type
    lineChart1 = pygal.Line(show_x_labels=False)
    lineChart1.title = f'US National Average Price $ by Type - {df["Date"].min()} to {df["Date"].max()}'
    lineChart1.add('Avg', dfActualAVG['NationalAVG'])
    lineChart1.add('Organic Avg', dfTypeAVG.NationalAVG.loc[
        dfTypeAVG['type'] == 'organic'])
    lineChart1.add('Conventional Avg', dfTypeAVG.NationalAVG.loc[
        dfTypeAVG['type'] == 'conventional'])
    lineChart1.x_labels = map(str, dfActualAVG['Date'])
    lineChart1R = lineChart1.render_data_uri()

    # comparing the total volume of each type
    dfTypeCompare = dfTypeAVG.groupby(['type'])['Total Volume'].apply(
        sumFunc).reset_index().sort_values(by=['type'])

    # seasonality
    dfYearMon = df[['Date','Total Volume','year','Total$']]
    dfYearMon['Month'] = dfYearMon.Date.map(splitFunc)
    dfYearMonTot = dfYearMon.groupby(['year', 'Month'])['Total Volume','Total$'].apply(
        sumFunc).sort_values(by=['year','Month']).reset_index()
    dfYearMonTot['MonthlyAvg'] = dfYearMonTot['Total$']/dfYearMonTot['Total Volume']
    # seasonality avg
    dfYearMonAvg = dfYearMon.groupby(['Month'])['Total Volume','Total$'].apply(
    sumFunc).sort_values(by=['Month']).reset_index()
    dfYearMonAvg['MonthlyAvg'] = dfYearMonAvg['Total$']/dfYearMonAvg['Total Volume']
    lineChart2 = pygal.Line()
    lineChart2.title = f'Monthly Avg Prices $ vs Total Volume - {dfYearMonTot.year.min()}-{dfYearMonTot.year.max()}'
    lineChart2.add('Average Price AP', dfYearMonAvg.MonthlyAvg)
    lineChart2.add('Total Volume TV', dfYearMonAvg['Total Volume'].values, secondary=True)
    lineChart2.x_labels = map(str, dfYearMonTot['Month'].unique())
    lineChart2R = lineChart2.render_data_uri()

    lineChart3 = pygal.Line()
    lineChart3.title = f'Monthly Avg $ and Total Volume - {dfYearMonTot.year.min()}-{dfYearMonTot.year.max()} by Year'
    lineChart3.add('2015 AP', dfYearMonTot.MonthlyAvg.loc[dfYearMonTot.year == 2015])
    lineChart3.add('2016 AP', dfYearMonTot.MonthlyAvg.loc[dfYearMonTot.year == 2016])
    lineChart3.add('2017 AP', dfYearMonTot.MonthlyAvg.loc[dfYearMonTot.year == 2017])
    lineChart3.add('2018 AP', dfYearMonTot.MonthlyAvg.loc[dfYearMonTot.year == 2018])
    lineChart3.add('2015 TV', dfYearMonTot['Total Volume'].loc[dfYearMonTot.year == 2015], secondary=True)
    lineChart3.add('2016 TV', dfYearMonTot['Total Volume'].loc[dfYearMonTot.year == 2016], secondary=True)
    lineChart3.add('2017 TV', dfYearMonTot['Total Volume'].loc[dfYearMonTot.year == 2017], secondary=True)
    lineChart3.add('2018 TV', dfYearMonTot['Total Volume'].loc[dfYearMonTot.year == 2018], secondary=True)
    lineChart3.x_labels = map(str, dfYearMonTot['Month'].unique())
    lineChart3R = lineChart3.render_data_uri()

    dfHighPriceReg = df[['region','Date']].loc[df.AveragePrice == df['AveragePrice'].max()].values[0]

    return {
        'df': df,
        'uniqueRegion': df.region.unique(),
        'dateMin': df['Date'].min(),
        'dateMax': df['Date'].max(),
        'avgPriceMin': df['AveragePrice'].min(),
        'avgPriceMax': df['AveragePrice'].max(),
        'mean': df['AveragePrice'].mean(),
        'std': df['AveragePrice'].std(),
        'boxPlotR': boxPlotR,
        'lineChart1R': lineChart1R,
        'conTot': dfTypeCompare["Total Volume"][0],
        'orgTot': dfTypeCompare["Total Volume"][1],
        'preAggedRegions': preAggedRegions,
        'lineChart2R': lineChart2R,
        'avgPrice': avgPrice,
        'lineChart3R': lineChart3R,
        'dfHighPriceReg': dfHighPriceReg,
        'organicAP': organicAP,
        'convAP': convAP
    }


def getIrisData():
    colNames = ['sepal-length', 'sepal-width', 'petal-length', 'petal-width', 'class']
    irisData = pd.read_csv(current_app.config['IRIS_DATA'], names=colNames)

    boxPlot = pygal.Box()
    boxPlot.title = 'Iris Dataset Boxplots'
    boxPlot.add('sepal-length', irisData['sepal-length'])
    boxPlot.add('sepal-width', irisData['sepal-width'])
    boxPlot.add('petal-length', irisData['petal-length'])
    boxPlot.add('petal-width', irisData['petal-width'])
    boxPlot = boxPlot.render_data_uri()

    hist = pygal.Histogram()
    hist.add('sepal-length', irisData['sepal-length'])
    hist.add('sepal-width', irisData['sepal-width'])
    hist.add('petal-length', irisData['petal-length'])
    hist.add('petal-width', irisData['petal-width'])
    hist = hist.render_data_uri()

    return {
        'boxPlot': boxPlot,
        'hist': hist
    }


def getYoutubeVids(searchTerm, numOfResults=20, vidLength='short'):
    youtubeKey = conf['APIKEYS']['googleYoutubeKey']
    service = build('youtube', 'v3', developerKey=youtubeKey)
    userSearch = service.search().list(q=searchTerm, part='snippet',
                                       type='video', maxResults=numOfResults,
                                       videoDuration=vidLength)
    ytResults = userSearch.execute()
    return ytResults


def getFlikrImgs(searchTerm):
    flikrURL = f'''
    https://www.flickr.com/services/feeds/photos_public.gne?format=json&tags={searchTerm}
    '''  # noqa
    r = requests.get(flikrURL)
    if r.status_code == 200:
        flikrResults = dict(loads(r.text[:-1].strip('jsonFlickrFeed(')))
        return flikrResults
    else:
        return None


def keyCheck(key):
    if key == '6d606332-6b8d-45a7-b2b3-01520e02a6fc':
        return True
    else:
        return False
